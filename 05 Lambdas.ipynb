{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 1: Anonymous Functions (lambda)\n",
    "\n",
    "You have seen how functional programming allows you to write functions that can be used for mapping and reducing data. However, to date, this hasn’t saved you from typing much code. This is because you have had to declare every function that you want to use, i.e. you had to use syntax such as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(x, y):\n",
    "    return x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "to both provide the code to the function (`return x + y`) and also to assign that function to an initial variable (`add`).\n",
    "\n",
    "Anonymous functions (also called lambdas) allow you to declare the code for functions without having to assign them to a variable. They are used for short, one-line functions, such as the `add` function used above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[7, 9, 11, 13, 15]\n"
     ]
    }
   ],
   "source": [
    "a = [1, 2, 3, 4, 5]\n",
    "b = [6, 7, 8, 9, 10]\n",
    "\n",
    "total = map(lambda x, y: x + y, a, b)\n",
    "\n",
    "print(list(total))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This code has used `lambda` to create an anonymous function that is passed as an argument to `map`. The format of `lambda` is:\n",
    "\n",
    "```python\n",
    "lambda arguments: expression\n",
    "```\n",
    "\n",
    "where `arguments` is a comma separated list of arguments to the function, and `expression` is a single line of code. Note that this function will automatically return the result of this single line of code.\n",
    "\n",
    "Anonymous `lambda` functions are just like any other function. The only difference is that they have been created without being initially assigned to a variable. The unnamed function object created using\n",
    "\n",
    "```python\n",
    "lambda arguments: expression\n",
    "```\n",
    "\n",
    "is completely identical to\n",
    "\n",
    "```python\n",
    "def name(arguments):\n",
    "    return expression\n",
    "```\n",
    "\n",
    "except that the `def` version assigns this function object to a variable called `name`, while the `lambda` version creates the function object without assigning it to a variable.\n",
    "\n",
    "You use `lambda` whenever you want to pass a simple, one-line expression as an argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "120\n"
     ]
    }
   ],
   "source": [
    "from functools import reduce\n",
    "\n",
    "a = [1, 2, 3, 4, 5]\n",
    "\n",
    "product = reduce(lambda x, y: x * y, a)\n",
    "\n",
    "print(product)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 4, 9, 16, 25]\n"
     ]
    }
   ],
   "source": [
    "squares = map(lambda x: x * x, a)\n",
    "\n",
    "print(list(squares))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binding Arguments\n",
    "\n",
    "As well as using `lambda` to create functions as arguments, you can also use `lambda` to more quickly create simple functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "25\n"
     ]
    }
   ],
   "source": [
    "square = lambda x: x * x\n",
    "\n",
    "print(square(5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here you have created a simple function that accepts one argument, and returns that argument squared. You have immediately assigned this function to the variable `square`, allowing you to call this function via this variable.\n",
    "\n",
    "With `lambda`, you are limited to using this to create functions that have only a single expression (i.e. single line of code). However, this single expression can include a call to another function. This can allow you to quickly create specialised versions of more generic functions by binding their arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "12\n"
     ]
    }
   ],
   "source": [
    "def add(x, y):\n",
    "    \"\"\"Return the sum of the two arguments\"\"\"\n",
    "    return x + y\n",
    "\n",
    "plus_five = lambda x: add(x, 5)\n",
    "\n",
    "print(plus_five(7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we have created a new function that takes a single argument (`x`), and that only calls the function `add` with arguments `x` and `5`. This is assigned to the variable `plus_five`. This means that `plus_five` is now a function that takes a single argument, and returns the result of adding five to that argument.\n",
    "\n",
    "In this example, we have used `lambda` to bind the value of the second argument of `add` to the number `5`. The use of `lambda` has reduced the amount of code needed to create the `plus_five` function. Compare this to what is needed if we didn’t use lambda:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "12\n"
     ]
    }
   ],
   "source": [
    "def plus_five(x):\n",
    "    return add(x, 5)\n",
    "\n",
    "print(plus_five(7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The saving is more useful when we want to create specialised functions for mapping or reduction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2, 4, 6, 8, 10]\n"
     ]
    }
   ],
   "source": [
    "def multiply(x, y):\n",
    "    \"\"\"Return the product of the two arguments\"\"\"\n",
    "    return x * y\n",
    "\n",
    "a = [1, 2, 3, 4, 5]\n",
    "\n",
    "double_a = map(lambda x: multiply(x, 2), a)\n",
    "\n",
    "print(list(double_a))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Rewrite your `countlines.py` script so that it uses `lambda` instead of any defined function (e.g. you should replace the `count_lines` function).\n",
    "\n",
    "If you get stuck or want some inspiration, a possible answer is given [here](answer_shakespeare_lambda.ipynb)."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
