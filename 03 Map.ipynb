{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 1: Mapping Functions\n",
    "\n",
    "In many situations you would like to apply the same function to lots of different pieces of data. For example, let's create two arrays of numbers, and use our `add` function to add pairs of numbers together. In the Python Console type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(x, y):\n",
    "    \"\"\"Simple function returns the sum of the arguments\"\"\"\n",
    "    return x + y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def multiply(x, y):\n",
    "    \"\"\"\n",
    "    Simple function that returns the product of the\n",
    "    two arguments\n",
    "    \"\"\"\n",
    "    return x * y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[7, 9, 11, 13, 15]\n"
     ]
    }
   ],
   "source": [
    "a = [1, 2, 3, 4, 5]\n",
    "b = [6, 7, 8, 9, 10]\n",
    "\n",
    "result = []\n",
    "\n",
    "for i, j in zip(a, b):\n",
    "    result.append(add(i, j))\n",
    "\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above code has looped over every pair of numbers in the lists `a` and `b`, and has called the function `add` for each pair. Each result is appended to the list `result`, which is printed at the end of the loop.\n",
    "\n",
    "Applying the same function to every item in a list (or pair of lists) of data is really common. For example, in a molecular simulation, you may want to loop over a list of every molecule and call a `calculate_energy` function for each one. In a fluid dynamics simulation, you may want to loop over a list of grid points and call a `solve_gridpoint` function for each one. This pattern, of calling the same function for each element of a list (or set of lists) of data, is called mapping. In the above example, we have *mapped* the function `add` onto the lists `a` and `b`, returning `result`.\n",
    "\n",
    "The above code mapped the function `add`. How about if we wanted to map our `diff` or `multiply` functions? One option would be to copy out this code again. A better solution would be to use functional programming to write our own mapping function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mapper(func, arg1, arg2):\n",
    "    \"\"\"\n",
    "    This will map the function 'func' to each pair\n",
    "    of arguments in the list 'arg1' and 'arg2', returning\n",
    "    the result\n",
    "    \"\"\"\n",
    "\n",
    "    res = []\n",
    "\n",
    "    for i, j in zip(arg1, arg2):\n",
    "        res.append(func(i, j))\n",
    "\n",
    "    return res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[7, 9, 11, 13, 15]\n"
     ]
    }
   ],
   "source": [
    "result = mapper(add, a, b)\n",
    "\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[6, 14, 24, 36, 50]\n"
     ]
    }
   ],
   "source": [
    "result = mapper(multiply, a, b)\n",
    "\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can you see how this works?\n",
    "\n",
    "The `mapper` function takes as its first argument the function to be mapped. The other arguments are the two lists of data for the mapping. The part\n",
    "\n",
    "```python\n",
    "zip(arg1, arg2)\n",
    "```\n",
    "\n",
    "takes the two arguments and returns an interator which can go through them both at the same time. As soon as one of them runs out of elements, it will stop. The `mapper` function then loops through each of these pairs of data, calling `func` for each pair, and storing the result in the list `res`. This is then returned at the end.\n",
    "\n",
    "Because the `mapper` function calls the mapped function using the argument `func`, it can map any function that is passed to it, as long as that function accepts two arguments. For example, let us now create a completely different function to map:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "\n",
    "def calc_distance(point1, point2):\n",
    "    \"\"\"\n",
    "    Function to calculate and return the distance between\n",
    "    two points\n",
    "    \"\"\"\n",
    "\n",
    "    dx2 = (point1[0] - point2[0]) ** 2\n",
    "    dy2 = (point1[1] - point2[1]) ** 2\n",
    "    dz2 = (point1[2] - point2[2]) ** 2\n",
    "\n",
    "    return math.sqrt(dx2 + dy2 + dz2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This has created a function that calculates the distance between two points. Let’s now create two lists of points and use `mapper` to control the calculation of distances between points:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[5.196152422706632, 5.196152422706632, 5.196152422706632]\n"
     ]
    }
   ],
   "source": [
    "points1 = [(1.0, 1.0, 1.0), (2.0, 2.0, 2.0), (3.0, 3.0, 3.0)]\n",
    "points2 = [(4.0, 4.0, 4.0), (5.0, 5.0, 5.0), (6.0, 6.0, 6.0)]\n",
    "\n",
    "distances = mapper(calc_distance, points1, points2)\n",
    "\n",
    "print(distances)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Standard Map\n",
    "\n",
    "Mapping is so common and useful that it is built in as a standard Python function, called `map`. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<map object at 0x7f0d6c363b00>\n"
     ]
    }
   ],
   "source": [
    "distances = map(calc_distance, points1, points2)\n",
    "\n",
    "print(distances)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is perhaps a little unexpected as Python hasn’t actually given us the answer. Instead, the built-in `map` function has returned an object which is ready and waiting to perform the calculation you’ve asked. This can be useful because by evaluating the map “lazily”, you can avoid unnecessary computation. The technical term for the thing that has been returned is an *iterator*. You can use this object in a `for` loop just fine but you can only loop over it once.\n",
    "\n",
    "If you want to force Python to evaluate the map and give you the answers, you can turn it into a list usig the `list` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[5.196152422706632, 5.196152422706632, 5.196152422706632]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(distances)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should see that your `calc_distances` function has been mapped to all of the pairs of points.\n",
    "\n",
    "The standard `map` function behaves very similar to your hand-written `mapper` function, returing an iterator containing the result of applying your function to each item of data.\n",
    "\n",
    "One advantage of `map` is that it knows how to handle multiple arguments. For example, let’s create a function that only maps a single argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "def square(x):\n",
    "    \"\"\"\n",
    "    Simple function to return the square of\n",
    "    the passed argument\n",
    "    \"\"\"\n",
    "    return x * x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let’s try to use your handwritten `mapper` function to map `square` onto a list of numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "mapper() missing 1 required positional argument: 'arg2'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-12-1724fe4d2b8a>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0mnumbers\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;34m[\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m2\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m3\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m4\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m5\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      2\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 3\u001b[0;31m \u001b[0mresult\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mmapper\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0msquare\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mnumbers\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: mapper() missing 1 required positional argument: 'arg2'"
     ]
    }
   ],
   "source": [
    "numbers = [1, 2, 3, 4, 5]\n",
    "\n",
    "result = mapper(square, numbers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This raises an exception since we wrote our `mapper` function so that it mapped functions that expected *two* arguments. That meant that our `mapper` function needs three arguments; the mapped function plus two lists of arguments.\n",
    "\n",
    "The standard map function can handle different numbers of arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 4, 9, 16, 25]\n"
     ]
    }
   ],
   "source": [
    "result = map(square, numbers)\n",
    "\n",
    "print(list(result))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The standard `map` function can work with mapping functions that accept any number of arguments. If the mapping function accepts `n` arguments, then you must pass `n+1` arguments to map, i.e. the mapped function, plus `n` lists of arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 2, 1, 2, 1]\n"
     ]
    }
   ],
   "source": [
    "def find_smallest(arg1, arg2, arg3):\n",
    "    \"\"\"\n",
    "    Function used to return the smallest value out \n",
    "    of 'arg1', 'arg2' and 'arg3'\n",
    "    \"\"\"\n",
    "\n",
    "    return min(arg1, arg2, arg3)\n",
    "\n",
    "a = [1, 2, 3, 4, 5]\n",
    "b = [5, 4, 3, 2, 1]\n",
    "c = [1, 2, 1, 2, 1]\n",
    "\n",
    "result = map(find_smallest, a, b, c)\n",
    "\n",
    "print(list(result))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is this output what you expect?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Download and unpack the file `shakespeare.tar.bz2`, e.g. type into a Python Console:\n",
    "\n",
    "```python\n",
    "import urllib\n",
    "import tarfile\n",
    "\n",
    "urllib.request.urlretrieve(\"https://gitlab.com/milliams/parallel_python/raw/master/shakespeare.tar.bz2\", \"shakespeare.tar.bz2\")\n",
    "with tarfile.open(\"shakespeare.tar.bz2\") as tar:\n",
    "    tar.extractall()\n",
    "```\n",
    "\n",
    "This has created a directory called `shakespeare` that contains the full text of many of Shakespeare’s plays.\n",
    "\n",
    "Your task is to write a Python script, called `countlines.py`, that will count the total number of lines in each of these Shakespeare plays, e.g. by using the command line call\n",
    "\n",
    "```bash\n",
    "python countlines.py shakespeare\n",
    "```\n",
    "\n",
    "To do this, first write a function that counts the number of lines in a file.\n",
    "\n",
    "Then, use the standard `map` function to count the number of lines in each Shakespeare play, printing the result as a list.\n",
    "\n",
    "If you get stuck or want some inspiration, a possible answer is given [here](answer_shakespeare_map.ipynb)."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
