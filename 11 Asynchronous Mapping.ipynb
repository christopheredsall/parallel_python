{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Asynchronous Mapping\n",
    "\n",
    "Asynchronous functions allow you to give different tasks to different members of the `multiprocessing.Pool`. However, giving functions one by one is not very efficient. It would be good to be able to combine mapping with asynchronous functions, i.e. be able to give different mapping tasks simultanously to the pool of workers. Fortunately, `Pool.map_async` provides exactly that - an asynchronous parallel map.\n",
    "\n",
    "Create a new python script called `asyncmap.py` and copy into it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing asyncmap.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile asyncmap.py\n",
    "\n",
    "from functools import reduce\n",
    "from multiprocessing import Pool, current_process\n",
    "import time\n",
    "\n",
    "def add(x, y):\n",
    "    \"\"\"Return the sum of the arguments\"\"\"\n",
    "    print(f\"Worker {current_process().pid} is processing add({x}, {y})\")\n",
    "    time.sleep(1)\n",
    "    return x + y\n",
    "\n",
    "def product(x, y):\n",
    "    \"\"\"Return the product of the arguments\"\"\"\n",
    "    print(f\"Worker {current_process().pid} is processing product({x}, {y})\")\n",
    "    time.sleep(1)\n",
    "    return x * y\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "\n",
    "    a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]\n",
    "    b = [11, 12, 13, 14, 15, 16, 17, 18, 19, 20]\n",
    "\n",
    "    # Now create a Pool of workers\n",
    "    with Pool() as pool:\n",
    "        sum_future = pool.starmap_async(add, zip(a,b))\n",
    "        product_future = pool.starmap_async(product, zip(a,b))\n",
    "\n",
    "        sum_future.wait()\n",
    "        product_future.wait()\n",
    "\n",
    "    total_sum = reduce(lambda x, y: x + y, sum_future.get())\n",
    "    total_product = reduce(lambda x, y: x + y, product_future.get())\n",
    "\n",
    "    print(f\"Sum of sums of 'a' and 'b' is {total_sum}\")\n",
    "    print(f\"Sum of products of 'a' and 'b' is {total_product}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running this script using should give output similar to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Worker 17038 is processing add(1, 11)\n",
      "Worker 17039 is processing add(2, 12)\n",
      "Worker 17041 is processing add(3, 13)\n",
      "Worker 17040 is processing add(4, 14)\n",
      "Worker 17041 is processing add(5, 15)\n",
      "Worker 17038 is processing add(6, 16)\n",
      "Worker 17039 is processing add(7, 17)\n",
      "Worker 17040 is processing add(8, 18)\n",
      "Worker 17039 is processing add(9, 19)\n",
      "Worker 17040 is processing add(10, 20)\n",
      "Worker 17041 is processing product(1, 11)\n",
      "Worker 17038 is processing product(2, 12)\n",
      "Worker 17039 is processing product(3, 13)\n",
      "Worker 17040 is processing product(4, 14)\n",
      "Worker 17041 is processing product(5, 15)\n",
      "Worker 17038 is processing product(6, 16)\n",
      "Worker 17039 is processing product(7, 17)\n",
      "Worker 17040 is processing product(8, 18)\n",
      "Worker 17038 is processing product(9, 19)\n",
      "Worker 17041 is processing product(10, 20)\n",
      "Sum of sums of 'a' and 'b' is 210\n",
      "Sum of products of 'a' and 'b' is 935\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python asyncmap.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This script provides two functions, `add` and `product`, which are mapped asynchronously using the `Pool.map_async` function. This is identical to the `Pool.map` function that you used before, except now the map is performed asynchronously. This means that the resulting list is returned in a future (in this case, the futures `sum_future` and `product_future`. The results are waited for using the `.wait()` functions, remembering to make sure that we don’t exit the `with` block until all results are available. Then, the results of mapping are retrieved using the `.get()` function of the futures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Chunking\n",
    "\n",
    "By default, the `Pool.map` function divides the work over the pool of workers by assiging pieces of work one by one. In the example above, the work to be performed was:\n",
    "\n",
    "```python\n",
    "add(1, 11)\n",
    "add(2, 12)\n",
    "add(3, 13)\n",
    "etc.\n",
    "add(10,20)\n",
    "product(1, 11)\n",
    "product(2, 12)\n",
    "product(3, 13)\n",
    "etc.\n",
    "product(10, 20)\n",
    "```\n",
    "\n",
    "The work was assigned one by one to the four workers on my computer, i.e. the first worker process was given `add(1, 11)`, the second `add(2, 12)`, the third `add(3, 13)` the then the fourth `add(4, 14)`. The first worker to finish was then given `add(5, 15)`, then the next given `add(6, 16)` etc. etc.\n",
    "\n",
    "Giving work one by one can be very inefficient for quick tasks, as the time needed by a worker process to stop and get new work can be longer than it takes to actually complete the task. To solve this problem, you can control how many work items are handed out to each worker process at a time. This is known as chunking, and the number of work items is known as the *chunk* of work to perform.\n",
    "\n",
    "You can control the number of work items to perform per worker (the *chunk size*) by setting the `chunksize` argument, e.g.\n",
    "\n",
    "```python\n",
    "sum_future = pool.starmap_async(add, zip(a, b), chunksize=5)\n",
    "```\n",
    "\n",
    "would suggest to `pool` that each worker be given a chunk of five pieces of work. Note that this is just a suggestion, and pool may decide to use a slightly smaller or larger chunk size depending on the amount of work and the number of workers available.\n",
    "\n",
    "Modify your `asyncmap.py` script and set the `chunksize` to `5` for both of the asynchronous maps for `add` and `product`. Re-run your script. You should see something like;\n",
    "\n",
    "```\n",
    "Worker 658 is processing add(1, 11)\n",
    "Worker 659 is processing add(6, 16)\n",
    "Worker 660 is processing product(1, 11)\n",
    "Worker 661 is processing product(6, 16)\n",
    "Worker 659 is processing add(7, 17)\n",
    "Worker 660 is processing product(2, 12)\n",
    "Worker 661 is processing product(7, 17)\n",
    "Worker 658 is processing add(2, 12)\n",
    "Worker 660 is processing product(3, 13)\n",
    "Worker 659 is processing add(8, 18)\n",
    "Worker 661 is processing product(8, 18)\n",
    "Worker 658 is processing add(3, 13)\n",
    "Worker 660 is processing product(4, 14)\n",
    "Worker 659 is processing add(9, 19)\n",
    "Worker 661 is processing product(9, 19)\n",
    "Worker 658 is processing add(4, 14)\n",
    "Worker 659 is processing add(10, 20)\n",
    "Worker 660 is processing product(5, 15)\n",
    "Worker 661 is processing product(10, 20)\n",
    "Worker 658 is processing add(5, 15)\n",
    "Sum of sums of 'a' and 'b' is 210\n",
    "Sum of products of 'a' and 'b' is 935\n",
    "```\n",
    "\n",
    "My laptop has four workers. The first worker is assigned the first five items of work, i.e. `add(1, 11)` to `add(5, 15)`, and it starts by running `add(1, 11)`, hence why `add(1, 11)` is printed first.\n",
    "\n",
    "The next worker is given the next five items of work, i.e. `add(6, 16)` to `add(10,20)`, and starts by running `add(6, 16)`, hence why `add(6, 16)` is printed second.\n",
    "\n",
    "The next worker is given the next five items of work, i.e. `product(1, 11)` to `product(5, 15)`, and it starts by running `product(1, 11)`, hence why this is printed third.\n",
    "\n",
    "The last worker is given the next five items of work, i.e. `product(6, 16)` to `product(10, 20)`, and it starts by running `product(6, 16)`, hence why this is printed fourth.\n",
    "\n",
    "Once each worker has finished its first item of work, it moves onto its second. This is why `add(2, 12)`, `add(7, 17)`, `product(2, 12)` and `product(7, 17)` are printed next. Then, each worker moves onto its third piece of work etc. etc.\n",
    "\n",
    "If you don’t specify the chunksize then it is equal to `1`. When writing a new script you should experiment with different values of chunksize to find the value that gives best performance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Edit your script written in answer to exercise 2 of [Parallel Map/Reduce](09%20Map%20Reduce.ipynb), in which you count all of the words used in all Shakespeare plays in `coundwords.py` (e.g. an example answer is [here](answer_shakespeare_countwords.ipynb)).\n",
    "\n",
    "Edit the script so that you use an asynchronous map to distribute the work over the pool. This will free up the master process to give feedback to the user of the script, e.g. to print a progress or status message while the work is running to reassure the user that the script has not frozen. For example\n",
    "\n",
    "```python\n",
    "while not future.ready():\n",
    "    print(\"Work is in progress...\")\n",
    "    time.sleep(0.1)\n",
    "```\n",
    "\n",
    "Add a status message to your script to reassure the user that your script hasn’t frozen while it is processing.\n",
    "\n",
    "(note that you can call your script using `python -u countwords.py shakespeare/*` to use the `-u` argument to stop Python from buffering text written to standard output)\n",
    "\n",
    "If you get stuck or want inspiration, a possible answer is given [here](answer_shakespeare_countwords_async.ipynb)."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
