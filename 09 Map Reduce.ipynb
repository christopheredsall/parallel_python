{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Parallel map/reduce\n",
    "\n",
    "The `multiprocessing.Pool` provides an excellent mechanism for the parallelisation of map/reduce style calculations. However, there are a number of caveats that make it more difficult to use than the simple map/reduce that was introduced in [Part 1](03%20Map.ipynb).\n",
    "Mapping functions with multiple arguments\n",
    "\n",
    "The `Pool.map` function only supports mapping functions that have a single argument. This means that if you want to map over a function which expects multiple arguments you can’t use it. Instead, you can use `Pool.starmap` which expects you to pass it a list of tuples where each tuple will be unpacked and passed to the function.\n",
    "\n",
    "For example:\n",
    "\n",
    "```python\n",
    "args = [(1, 6), (2, 7), (3, 8)]\n",
    "pool.starmap(add, args)\n",
    "```\n",
    "\n",
    "will effectively return:\n",
    "\n",
    "```python\n",
    "[add(1, 6), add(2, 7), add(3, 8)]\n",
    "```\n",
    "\n",
    "The above trick allows you to use any multiple-argument function. However, doing this now means that you have to convert multiple lists of arguments into a single list of multiple arguments. For example, we need to convert\n",
    "\n",
    "```python\n",
    "def add(x, y):\n",
    "    \"\"\"Return the sum of the two arguments\"\"\"\n",
    "    return x + y\n",
    "\n",
    "a = [1, 2, 3, 4, 5]\n",
    "b = [6, 7, 8, 9, 10]\n",
    "\n",
    "result = map(add, a, b)\n",
    "print(list(result))\n",
    "```\n",
    "\n",
    "to\n",
    "\n",
    "```python\n",
    "from multiprocessing import Pool\n",
    "\n",
    "def add(x, y):\n",
    "    \"\"\"Return the sum of the tuple of two arguments\"\"\"\n",
    "    return x + y\n",
    "\n",
    "a_b = [(1,6), (2,7), (3,8), (4,9), (5,10)]\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    with Pool() as pool:\n",
    "        result = pool.starmap(add, a_b)\n",
    "\n",
    "    print(result)\n",
    "```\n",
    "\n",
    "Combining the two lists of arguments into a single list of tuples could be painful. Fortunately, python provides the zip function. This automatically zips up N lists into one list of tuples (each tuple containing N items). For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[(1, 6, 11), (2, 7, 12), (3, 8, 13), (4, 9, 14), (5, 10, 15)]\n"
     ]
    }
   ],
   "source": [
    "a = [1, 2, 3, 4, 5]\n",
    "b = [6, 7, 8, 9, 10]\n",
    "c = [11, 12, 13, 14, 15]\n",
    "\n",
    "args = zip(a, b, c)\n",
    "print(list(args))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You thus need to use zip to zip together the arguments when you call Pool.starmap. For example, the above example should be written\n",
    "\n",
    "```python\n",
    "from multiprocessing import Pool\n",
    "\n",
    "def add(x, y):\n",
    "    \"\"\"Return the sum of the tuple of two arguments\"\"\"\n",
    "    return x + y\n",
    "\n",
    "a = [1, 2, 3, 4, 5]\n",
    "b = [6, 7, 8, 9, 10]\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    with Pool() as pool:\n",
    "        result = pool.starmap(add, zip(a, b))\n",
    "\n",
    "    print(result)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multiprocessing doesn’t (yet) support anonymous (lambda) functions\n",
    "\n",
    "One of the annoying limitations of the current version of `multiprocessing` is that it does not support anonymous (`lambda`) functions. The mapping function has to be created using the `def name(args)` syntax. The reason is because Python currently doesn’t pickle functions correctly (i.e. Python cannot convert the code of a function to a binary array of data that can be transmitted to the worker copies of the script. In contrast, Python can correctly pickle most argument types, so can send arguments to the workers). If you want to use anonymous functions with `multiprocessing`, then take a look at [this blog post](http://matthewrocklin.com/blog/work/2013/12/05/Parallelism-and-Serialization) for more information about the problems, and libraries that present possible solutions (e.g. the `dill` package has implemented its own fork of `multiprocessing` that can pickle functions, and so does support `lambda`s)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Edit your `countlines.py` script that you wrote for Part 1 so that you use `multiprocessing` to parallelise the counting of lines. Note that you will not be able to use `lambda` in the `pool.map` function.\n",
    "\n",
    "If you get stuck or want some inspiration, a possible answer is given [here](answer_shakespeare_multi.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Below are two functions. The first counts the number of times every word in a file appears in that file, returning the result as a dictionary (the key is the word, the value is the number of times it appears). The second function combines (reduces) two dictionaries together.\n",
    "\n",
    "```python\n",
    "import re\n",
    "\n",
    "\n",
    "def count_words(filename):\n",
    "    \"\"\"\n",
    "    Count the number of times every word in the file `filename`\n",
    "    is contained in this file.\n",
    "\n",
    "    Args:\n",
    "        filename (str): the filename to count the words in\n",
    "\n",
    "    Returns:\n",
    "        dict: a mapping of word to count\n",
    "    \"\"\"\n",
    "\n",
    "    all_words = {}\n",
    "\n",
    "    with open(filename) as f:\n",
    "        for line in f:\n",
    "            words = line.split()\n",
    "\n",
    "            for word in words:\n",
    "                #lowercase the word and remove all\n",
    "                #characters that are not [a-z] or hyphen\n",
    "                word = word.lower()\n",
    "                match = re.search(r\"([a-z\\-]+)\", word)\n",
    "\n",
    "                if match:\n",
    "                    word = match.groups()[0]\n",
    "\n",
    "                    if word in all_words:\n",
    "                        all_words[word] += 1\n",
    "                    else:\n",
    "                        all_words[word] = 1\n",
    "\n",
    "    return all_words\n",
    "\n",
    "\n",
    "def reduce_dicts(dict1, dict2):\n",
    "    \"\"\"\n",
    "    Combine (reduce) the passed two dictionaries to return\n",
    "    a dictionary that contains the keys of both, where the\n",
    "    values are equal to the sum of values for each key\n",
    "    \"\"\"\n",
    "\n",
    "    # explicitly copy the dictionary, as otherwise\n",
    "    # we risk modifying 'dict1'\n",
    "    combined = {}\n",
    "\n",
    "    for key in dict1:\n",
    "        combined[key] = dict1[key]\n",
    "\n",
    "    for key in dict2:\n",
    "        if key in combined:\n",
    "            combined[key] += dict2[key]\n",
    "        else:\n",
    "            combined[key] = dict2[key]\n",
    "\n",
    "    return combined\n",
    "```\n",
    "\n",
    "Use the above two function to write a parallel Python script called `countwords.py` that counts how many times each word used by Shakespeare appears in all of his plays, e.g. by using the command line call\n",
    "\n",
    "```bash\n",
    "python countwords.py shakespeare\n",
    "```\n",
    "\n",
    "Have your script print out every word that appears more than 2000 times across all of the plays. The words should be printed out in alphabetical order, and printed together with the number of times that they are used.\n",
    "\n",
    "If you get stuck or want some inspiration, a possible answer is given [here](answer_shakespeare_countwords.ipynb)."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
