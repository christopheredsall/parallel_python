{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Pool\n",
    "\n",
    "One of the core `multiprocessing` features is `multiprocessing.Pool`. This provides a pool of workers that can be used to parallelise a `map`.\n",
    "\n",
    "For example, create a new script called `pool.py` and type into it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting pool.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile pool.py\n",
    "\n",
    "from functools import reduce\n",
    "from multiprocessing import Pool, cpu_count\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Function to return the square of the argument\"\"\"\n",
    "    return x * x\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    # print the number of cores\n",
    "    print(f\"Number of cores available equals {cpu_count()}\")\n",
    "\n",
    "    # create a pool of workers\n",
    "    with Pool() as pool:\n",
    "        # create an array of 5000 integers, from 1 to 5000\n",
    "        r = range(1, 5001)\n",
    "\n",
    "        result = pool.map(square, r)\n",
    "\n",
    "    total = reduce(lambda x, y: x + y, result)\n",
    "\n",
    "    print(f\"The sum of the square of the first 5000 integers is {total}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the script using the command in the Terminal:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of cores available equals 4\n",
      "The sum of the square of the first 5000 integers is 41679167500\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python pool.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(the number of cores will depend on the number available on your machine)\n",
    "\n",
    "So how does this work? The line\n",
    "\n",
    "```python\n",
    "with Pool() as pool:\n",
    "```\n",
    "\n",
    "has created a pool of worker copies of your script, with the number of workers equalling the number of cores reported by `cpu_count()`. You can control the number of copies by specifying the value of processes in the constructor for `Pool`, e.g.\n",
    "\n",
    "```python\n",
    "with Pool(processes=5) as pool:\n",
    "```\n",
    "\n",
    "would create a pool of five workers.\n",
    "\n",
    "The line\n",
    "\n",
    "```python\n",
    "r = range(1,5001)\n",
    "```\n",
    "\n",
    "is a quick way to create a list of 5000 integers, from 1 to 5000. The parallel work is conducted on the line\n",
    "\n",
    "```python\n",
    "result = pool.map(square, r)\n",
    "```\n",
    "\n",
    "This performs a map of the function `square` over the list of items in `r`. The `map` is divided up over all of the workers in the pool. This means that, if you have 10 workers (e.g. if you have 10 cores), then each worker will perform only one tenth of the work (e.g. calculating the square of 500 numbers). If you have 2 workers, then each worker will perform only half of the work (e.g. calculating the square of 2500 numbers).\n",
    "\n",
    "The next line\n",
    "\n",
    "```python\n",
    "total = reduce(lambda x, y: x + y, result)\n",
    "```\n",
    "\n",
    "is just a standard reduce used to sum together all of the results.\n",
    "\n",
    "You can verify that the square function is divided between your workers by using a `multiprocessing.current_process().pid` call, which will return the process ID (PID) of the worker process. Edit your `pool.py` script and set the contents equal to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting pool.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile pool.py\n",
    "\n",
    "from functools import reduce\n",
    "from multiprocessing import Pool, current_process\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Function to return the square of the argument\"\"\"\n",
    "    print(f\"Worker {current_process().pid} calculating square of {x}\")\n",
    "    return x * x\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    nprocs = 2\n",
    "\n",
    "    # print the number of cores\n",
    "    print(f\"Number of workers equals {nprocs}\")\n",
    "\n",
    "    # create a pool of workers\n",
    "    with Pool(processes=nprocs) as pool:\n",
    "        # create an array of 5000 integers, from 1 to 5000\n",
    "        r = range(1, 21)\n",
    "\n",
    "        result = pool.map(square, r)\n",
    "\n",
    "    total = reduce(lambda x, y: x + y, result)\n",
    "\n",
    "    print(f\"The sum of the square of the first 20 integers is {total}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run this script using"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of workers equals 2\n",
      "Worker 31089 calculating square of 1\n",
      "Worker 31089 calculating square of 2\n",
      "Worker 31089 calculating square of 3\n",
      "Worker 31090 calculating square of 4\n",
      "Worker 31090 calculating square of 5\n",
      "Worker 31090 calculating square of 6\n",
      "Worker 31089 calculating square of 7\n",
      "Worker 31089 calculating square of 8\n",
      "Worker 31089 calculating square of 9\n",
      "Worker 31090 calculating square of 10\n",
      "Worker 31090 calculating square of 11\n",
      "Worker 31089 calculating square of 13\n",
      "Worker 31090 calculating square of 12\n",
      "Worker 31089 calculating square of 14\n",
      "Worker 31089 calculating square of 15\n",
      "Worker 31090 calculating square of 16\n",
      "Worker 31090 calculating square of 17\n",
      "Worker 31090 calculating square of 18\n",
      "Worker 31089 calculating square of 19\n",
      "Worker 31089 calculating square of 20\n",
      "The sum of the square of the first 20 integers is 2870\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python pool.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(the exact PIDs of the workers, and the order in which they print will be different on your machine)\n",
    "\n",
    "You can see in the output that there are two workers, signified by the two different worker PIDs. The work has been divided evenly amongst them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Edit `pool.py` and change the value of `nprocs`. How is the work divided as you change the number of workers?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using multiple pools in a single script\n",
    "\n",
    "You can use more than one `multiprocessing.Pool` at a time in your script, but you should ensure that you use them one after another. The way `multiprocessing.Pool` works is to fork your script into the team of workers when you create a `Pool` object. Each worker contains a complete copy of all of the functions and variables that exist at the time of the fork. This means that any changes after the fork will not be held by the other workers.\n",
    "\n",
    "If you made a Python script called `broken_pool.py` with the contents:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting broken_pool.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile broken_pool.py\n",
    "\n",
    "from multiprocessing import Pool\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Return the square of the argument\"\"\"\n",
    "    return x * x\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "\n",
    "    r = [1, 2, 3, 4, 5]\n",
    "\n",
    "    with Pool() as pool:\n",
    "        result = pool.map(square, r)\n",
    "\n",
    "        print(\"Square result: {result}\")\n",
    "\n",
    "        def cube(x):\n",
    "            \"\"\"Return the cube of the argument\"\"\"\n",
    "            return x * x * x\n",
    "\n",
    "        result = pool.map(cube, r)\n",
    "\n",
    "        print(\"Cube result: {result}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and ran it you would see an error like:\n",
    "\n",
    "```\n",
    "AttributeError: Can't get attribute 'cube' on <module '__main__' from 'broken_pool.py'>\n",
    "```\n",
    "\n",
    "The problem is that `pool` was created before the `cube` function. The worker copies of the script were thus created before `cube` was defined, and so don’t contain a copy of this function. This is one of the reasons why you should always define your functions above the `if __name__ == \"__main__\"` block.\n",
    "\n",
    "Alternatively, if you have to define the function in the `__main__` block, then ensure that you create the pool after the definition. For example, one fix here is to create a second pool for the second map:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting pool.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile pool.py\n",
    "\n",
    "from multiprocessing import Pool\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Return the square of the argument\"\"\"\n",
    "    return x * x\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "\n",
    "    r = [1, 2, 3, 4, 5]\n",
    "\n",
    "    with Pool() as pool:\n",
    "        result = pool.map(square, r)\n",
    "\n",
    "        print(f\"Square result: {result}\")\n",
    "\n",
    "    def cube(x):\n",
    "        \"\"\"Return the cube of the argument\"\"\"\n",
    "        return x * x * x\n",
    "\n",
    "    with Pool() as pool:\n",
    "        result = pool.map(cube, r)\n",
    "\n",
    "        print(f\"Cube result: {result}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running this should print out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Square result: [1, 4, 9, 16, 25]\n",
      "Cube result: [1, 8, 27, 64, 125]\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python pool.py"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
