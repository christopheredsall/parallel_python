{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Asynchronous Functions and Futures\n",
    "\n",
    "The `Pool.map` function allows you to map a single function across an entire list of data. But what if you want to apply lots of different functions? The solution is to tell individual workers to run different functions, by *applying* functions to workers.\n",
    "\n",
    "The `Pool` class comes with the function `apply`. This is used to tell one process in the worker pool to run a specified function. For example, create a new script called `poolapply.py` and type into it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing poolapply.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile poolapply.py\n",
    "\n",
    "import time\n",
    "from multiprocessing import Pool, current_process\n",
    "\n",
    "def slow_function(nsecs):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, returning\n",
    "    the number of seconds that it slept\n",
    "    \"\"\"\n",
    "\n",
    "    print(f\"Process {current_process().pid} going to sleep for {nsecs} second(s)\")\n",
    "\n",
    "    # use the time.sleep function to sleep for nsecs seconds\n",
    "    time.sleep(nsecs)\n",
    "\n",
    "    print(f\"Process {current_process().pid} waking up\")\n",
    "\n",
    "    return nsecs\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    print(f\"Master process is PID {current_process().pid}\")\n",
    "\n",
    "    with Pool() as pool:\n",
    "        r = pool.apply(slow_function, [5])\n",
    "\n",
    "    print(f\"Result is {r}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run this script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Master process is PID 8997\n",
      "Process 8998 going to sleep for 5 second(s)\n",
      "Process 8998 waking up\n",
      "Result is 5\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python poolapply.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should see the something like this printed to the screen (with a delay of five seconds when the worker process sleeps).\n",
    "\n",
    "The key line in this script is:\n",
    "\n",
    "```python\n",
    "r = pool.apply(slow_function, [5])\n",
    "```\n",
    "\n",
    "The `pool.apply` function will request that one of the workers in the pool should run the passed function (in this case `slow_function`), with the arguments passed to the function held in the list (in this case `[5]`). The `pool.apply` function will wait until the passed function has finished, and will return the result of that function (here copied into `r`).\n",
    "\n",
    "The arguments to the applied function must be placed into a list. This is the case, even if the applied function has just a single argument (i.e. this is why we had to write `[5]` rather than just `5`. The list of arguments must contain the same number of arguments as needed by the applied function, in the same order as declared in the function. For example, edit your `poolapply.py` function to read:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting poolapply.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile poolapply.py\n",
    "\n",
    "import time\n",
    "from multiprocessing import Pool, current_process\n",
    "\n",
    "def slow_add(nsecs, x, y):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, and\n",
    "    then returns the sum of x and y\n",
    "    \"\"\"\n",
    "    print(f\"Process {current_process().pid} going to sleep for {nsecs} second(s)\")\n",
    "\n",
    "    time.sleep(nsecs)\n",
    "\n",
    "    print(f\"Process {current_process().pid} waking up\")\n",
    "\n",
    "    return x + y\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    print(f\"Master process is PID {current_process().pid}\")\n",
    "\n",
    "    with Pool() as pool:\n",
    "        r = pool.apply(slow_add, [1, 6, 7])\n",
    "\n",
    "    print(f\"Result is {r}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have edited `slow_function` to `slow_add`, with this function accepting three arguments. These three arguments are passed using the list in `pool.apply(slow_add, [1, 6, 7])`.\n",
    "\n",
    "Running this script using should give output similar to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Master process is PID 10125\n",
      "Process 10126 going to sleep for 1 second(s)\n",
      "Process 10126 waking up\n",
      "Result is 13\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python poolapply.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Asynchronous Functions\n",
    "\n",
    "A major problem of `Pool.apply` is that the master process is blocked until the worker has finished processing the applied function. This is obviously an issue if you want to run multiple applied functions in parallel!\n",
    "\n",
    "Fortunately, `Pool` has an `apply_async` function. This is an asynchronous version of `apply` that applies the function in a worker process, but without blocking the master. Create a new python script called `applyasync.py` and copy into it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing applyasync.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile applyasync.py\n",
    "\n",
    "import time\n",
    "from multiprocessing import Pool, current_process\n",
    "\n",
    "def slow_add(nsecs, x, y):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, and\n",
    "    then returns the sum of x and y\n",
    "    \"\"\"\n",
    "    print(f\"Process {current_process().pid} going to sleep for {nsecs} second(s)\")\n",
    "\n",
    "    time.sleep(nsecs)\n",
    "\n",
    "    print(f\"Process {current_process().pid} waking up\")\n",
    "\n",
    "    return x + y\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    print(f\"Master process is PID {current_process().pid}\")\n",
    "\n",
    "    with Pool() as pool:\n",
    "        r1 = pool.apply_async(slow_add, [1, 6, 7])\n",
    "        r2 = pool.apply_async(slow_add, [1, 2, 3])\n",
    "\n",
    "        r1.wait()\n",
    "        print(f\"Result one is {r1.get()}\")\n",
    "\n",
    "        r2.wait()\n",
    "        print(f\"Result two is {r2.get()}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running this script using should give output similar to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Master process is PID 11013\n",
      "Process 11021 going to sleep for 1 second(s)\n",
      "Process 11020 going to sleep for 1 second(s)\n",
      "Process 11020 waking up\n",
      "Process 11021 waking up\n",
      "Result one is 13\n",
      "Result two is 5\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python applyasync.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The keys lines of this script are\n",
    "\n",
    "```python\n",
    "r1 = pool.apply_async(slow_add, [1, 6, 7])\n",
    "r2 = pool.apply_async(slow_add, [1, 2, 3])\n",
    "```\n",
    "\n",
    "The `apply_async` function is identical to `apply`, except that it returns control to the master process immediately. This means that the master process is free to continue working (e.g. here, it `apply_async`s a second `slow_add` function). In this case, it allows us to run two `slow_sums` in parallel. Most noticeably here, even though each function call took one second to run, the whole program did not take two seconds. Due to running them in parallel, it finished the whole program in just over one second."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Futures\n",
    "\n",
    "An issue with running a function asynchronously is that the return value of the function is not available immediately. This means that, when running an asynchronous function, you don’t get the return value directly. Instead, `apply_async` returns a placeholder for the return value. This placeholder is called a “future”, and is a variable that in the future will be given the result of the function.\n",
    "\n",
    "Futures are a very common variable type in parallel programming across many languages. Futures provide several common functions:\n",
    "- Block (wait) until the result is available. In `multiprocessing`, this is via the `.wait()` function, e.g. `r1.wait()` in the above script.\n",
    "- Retrieve the result when it is available (blocking until it is available). This is the `.get()` function, e.g. `r1.get()`.\n",
    "- Test whether or not the result is available. This is the `.ready()` function, which returns `True` when the asynchronous function has finished and the result is available via `.get()`.\n",
    "- Test whether or not the function was a success, e.g. whether or not an exception was raised when running the function. This is the `.successful()` function, which returns True if the asynchronous function completed without raising an exception. Note that this function should only be called after the result is available (e.g. when `.ready()` returns `True`).\n",
    "\n",
    "In the above example, `r1` and `r2` were both futures for the results of the two asynchronous calls of `slow_sum`. The two `slow_sum` calls were processed by two worker processes. The master process was then blocked using `r1.wait()` to wait for the result of the first call, and then blocked using `r2.wait()` to wait or the result of the second call.\n",
    "\n",
    "(note that we had to wait for all of the results to be delivered to our futures before we exited the `with` block, or else the pool of workers could be destroyed before the functions have completed and the results are available)\n",
    "\n",
    "We can explore this more using the following example. Create a script called `future.py` and copy into it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing future.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile future.py\n",
    "\n",
    "import time\n",
    "from multiprocessing import Pool\n",
    "\n",
    "def slow_add(nsecs, x, y):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, and\n",
    "    then returns the sum of x and y\n",
    "    \"\"\"\n",
    "    time.sleep(nsecs)\n",
    "    return x + y\n",
    "\n",
    "def slow_diff(nsecs, x, y):\n",
    "    \"\"\"\n",
    "    Function that sleeps for 'nsecs' seconds, and\n",
    "    then retruns the difference of x and y\n",
    "    \"\"\"\n",
    "    time.sleep(nsecs)\n",
    "    return x - y\n",
    "\n",
    "def broken_function(nsecs):\n",
    "    \"\"\"Function that deliberately raises an AssertationError\"\"\"\n",
    "    time.sleep(nsecs)\n",
    "    raise ValueError(\"Called broken function\")\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    futures = []\n",
    "\n",
    "    with Pool() as pool:\n",
    "        futures.append(pool.apply_async(slow_add, [3, 6, 7]))\n",
    "        futures.append(pool.apply_async(slow_diff, [2, 5, 2]))\n",
    "        futures.append(pool.apply_async(slow_add, [1, 8, 1]))\n",
    "        futures.append(pool.apply_async(slow_diff, [5, 9, 2]))\n",
    "        futures.append(pool.apply_async(broken_function, [4]))\n",
    "\n",
    "        while True:\n",
    "            all_finished = True\n",
    "\n",
    "            print(\"\\nHave the workers finished?\")\n",
    "\n",
    "            for i, future in enumerate(futures):\n",
    "                if future.ready():\n",
    "                    print(f\"Process {i} has finished\")\n",
    "                else:\n",
    "                    all_finished = False\n",
    "                    print(f\"Process {i} is running...\")\n",
    "\n",
    "            if all_finished:\n",
    "                break\n",
    "\n",
    "            time.sleep(1)\n",
    "\n",
    "        print(\"\\nHere are the results.\")\n",
    "\n",
    "        for i, future in enumerate(futures):\n",
    "            if future.successful():\n",
    "                print(f\"Process {i} was successful. Result is {future.get()}\")\n",
    "            else:\n",
    "                print(f\"Process {i} failed!\")\n",
    "\n",
    "                try:\n",
    "                    future.get()\n",
    "                except Exception as e:\n",
    "                    print(f\"    Error = {type(e)} : {e}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running this script using should give output similar to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Have the workers finished?\n",
      "Process 0 is running...\n",
      "Process 1 is running...\n",
      "Process 2 is running...\n",
      "Process 3 is running...\n",
      "Process 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Process 0 is running...\n",
      "Process 1 is running...\n",
      "Process 2 is running...\n",
      "Process 3 is running...\n",
      "Process 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Process 0 is running...\n",
      "Process 1 is running...\n",
      "Process 2 has finished\n",
      "Process 3 is running...\n",
      "Process 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Process 0 has finished\n",
      "Process 1 has finished\n",
      "Process 2 has finished\n",
      "Process 3 is running...\n",
      "Process 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Process 0 has finished\n",
      "Process 1 has finished\n",
      "Process 2 has finished\n",
      "Process 3 is running...\n",
      "Process 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Process 0 has finished\n",
      "Process 1 has finished\n",
      "Process 2 has finished\n",
      "Process 3 is running...\n",
      "Process 4 is running...\n",
      "\n",
      "Have the workers finished?\n",
      "Process 0 has finished\n",
      "Process 1 has finished\n",
      "Process 2 has finished\n",
      "Process 3 has finished\n",
      "Process 4 has finished\n",
      "\n",
      "Here are the results.\n",
      "Process 0 was successful. Result is 13\n",
      "Process 1 was successful. Result is 3\n",
      "Process 2 was successful. Result is 9\n",
      "Process 3 was successful. Result is 7\n",
      "Process 4 failed!\n",
      "    Error = <class 'ValueError'> : Called broken function\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python future.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is this output that you expected? Note that the exception raised by `broken_function` is held safely in its associated future. This is indicated by `.successful()` returning `False`, thereby allowing us to handle the exception in a `try...except` block that is put around the `.get()` function (if you `.get()` a future that contains an exception, then that exception is raised)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Edit the `future.py` script so that you can control the number of workers in the pool using a command line argument (e.g. using `Pool(processes=int(sys.argv[1]))` rather than `Pool()`).\n",
    "\n",
    "Edit the script to add calls to more asynchronous functions.\n",
    "\n",
    "Then experiment with running the script with different numbers of processes in the pool and with different numbers of asynchronous function calls.\n",
    "\n",
    "How are the asynchronous function calls distributed across the pool of worker processes?"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
